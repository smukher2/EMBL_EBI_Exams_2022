/*The thing with getElementById is that it only allows to select an element by its id.
The querySelector method can be used in other situations as well, such as when
selecting by element name, nesting, or class name. In other words, the main
benefit of using querySelector or querySelectorAll is
that we can select elements using CSS selectors, which gives us a uniform way
of dealing with element selection, and that makes it a preferred way of
selecting elements to many developers.
Ref: https://beamtic.com/getelementbyid-vs-queryselector */

/*Sample graph code2
Ref: https://code.tutsplus.com/tutorials/how-to-draw-a-pie-chart-and-doughnut-chart-using-javascript-and-html5-canvas--cms-27197*/
var myCanvas = document.getElementById("myCanvasB");
myCanvas.width = 300;
myCanvas.height = 300;
var ctx = myCanvas.getContext("2d");
/*Drawing the bar chart only requires knowing how to draw two elements:
drawing a line: for drawing the grid lines
drawing a color-filled rectangle: for drawing the bars of the chart
We draw the line by calling beginPath(). This informs the drawing context that
we are starting to draw something new on the canvas. We use moveTo() to set the
starting point, call lineTo() to indicate the end point, and then do the actual
drawing by calling stroke().*/

//js object containing the data
var myVinyls = {
    "Classical music": 10,
    "Alternative rock": 14,
    "Pop": 2,
    "Jazz": 12
};

//helper function to draw
function drawLine(ctx, startX, startY, endX, endY){
    ctx.beginPath();
    ctx.moveTo(startX,startY);
    ctx.lineTo(endX,endY);
    ctx.stroke();
}
function drawArc(ctx, centerX, centerY, radius, startAngle, endAngle){
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.stroke();
}
function drawPieSlice(ctx,centerX, centerY, radius, startAngle, endAngle, color ){
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX,centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
}
//Here is an example for calling three functions:
drawLine(_ctx,100,100,200,200);
drawArc(_ctx, 150,150,150, 0, Math.PI/3);
drawPieSlice(_ctx, 150,150,150, Math.PI/2, Math.PI/2 + Math.PI/4, '#ff0000');

//pie chart drawing
var Piechart = function(options){
    this.options = options;
    this.canvas = options.canvas;
    this.ctx = this.canvas.getContext("2d");
    this.colors = options.colors;

    this.draw = function(){
        var total_value = 0;
        var color_index = 0;
        for (var categ in this.options.data){
            var val = this.options.data[categ];
            total_value += val;
        }

        var start_angle = 0;
        for (categ in this.options.data){
            val = this.options.data[categ];
            var slice_angle = 2 * Math.PI * val / total_value;

            drawPieSlice(
                this.ctx,
                this.canvas.width/2,
                this.canvas.height/2,
                Math.min(this.canvas.width/2,this.canvas.height/2),
                start_angle,
                start_angle+slice_angle,
                this.colors[color_index%this.colors.length]
            );

            start_angle += slice_angle;
            color_index++;
        }

    }
}

//applying bar plot from js to html file
var myPiechart = new Piechart(
    {
        canvas:myCanvas,
        data:myVinyls,
        colors:["#fde23e","#f16e23", "#57d9ff","#937e88"]
    }
);
myPiechart.draw();
